import { Request, Response, Router } from 'express';
import admin from '../firebaseAdmin';
// import { firestore } from 'firebase-admin'; // Explicit import for firestore

const db = admin.firestore();
const router = Router();

router.post('/', async (req: Request, res: Response) => {
	try {
		const { name, email, content } = req.body;

		if (!name || !email || !content) {
			console.error('Missing parameters in request body:', req.body);
			return res.status(400).send({ status: 'error', message: 'Missing required fields in request.' });
		}

		const writeResult = await db.collection('messages').add({
			name,
			email,
			content,
			// timestamp: firestore.Timestamp, // Using the explicitly imported firestore
			timestamp: new Date(),
		});

		console.log('Message stored with ID:', writeResult.id);
		res.status(200).send({ status: 'success' });
	} catch (error: any) {
		console.error('Error encountered while processing message:', error);
		res.status(500).send({ status: 'error', message: error.message });
	}
	return;
});

export default router;
