import * as functions from 'firebase-functions';
import express = require('express');
import cors = require('cors');
import messageRouter from './routes/message';

const app = express();

app.use(cors({ origin: true }));
app.use('/message', messageRouter);

app.get('/hello', (req, res) => {
	return res.status(200).send('Hello World!');
});

export const apiDeanPersonalWebsiteProduction = functions.https.onRequest(app);
