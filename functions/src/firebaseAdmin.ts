import * as admin from 'firebase-admin';
import serviceAccount from './service-account.json';

admin.initializeApp({
	credential: admin.credential.cert(serviceAccount as admin.ServiceAccount),
	// If you're also using Firebase Storage, specify its URL:
	// storageBucket: 'gs://website-75fd6.appspot.com/',
});

// let bucket: any;

// if (process.env.NODE_ENV !== 'production') {
// 	// Assuming you've set up Firebase Storage emulator locally:
// 	bucket = admin.storage().bucket('http://127.0.0.1:4000/storage/website-75fd6.appspot.com');
// } else {
// 	bucket = admin.storage().bucket();
// }

// export { bucket };
export default admin;
