interface CardProps {
	children: JSX.Element | JSX.Element[];
}

export const Card = (props: CardProps): JSX.Element => {
	return <div className='card'>{props.children}</div>;
};
