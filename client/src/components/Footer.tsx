export const Footer = (): JSX.Element => {
	const currentYear = new Date().getFullYear();
	return (
		<div className='footer'>
			<p>© {currentYear} Dean Rivers. All rights reserved.</p>
		</div>
	);
};
