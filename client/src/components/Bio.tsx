export const Bio = (): JSX.Element => {
	return (
		<div className='bio'>
			<p className='bio-header'>
				<b>Dean Rivers</b> - Software developer proficient in multiple programming languages, constantly
				innovating through an expansive toolkit, including Adobe's creative suite.
			</p>
			<p className='bio-keywords'>Develop - Create - Refine</p>
		</div>
	);
};
