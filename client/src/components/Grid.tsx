interface GridProps {
	children: JSX.Element;
}
export const Grid = (props: GridProps): JSX.Element => {
	return <div className='grid'>{props.children}</div>;
};
